#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <fcntl.h>

typedef struct 			s_client {
	int 				id;
	int 				socket;
	struct s_client* 	next;
}						t_client;

t_client* 				clients_list = NULL;

int 					server_socket = 0;
int 					nb_connection = 0;
int 					status = 0;
int 					ret = 0;

fd_set 					fds_read;
fd_set 					fds_write;
fd_set 					fds_current;

char 					notification[42];
char 					buffer_receptor[4096];

char* 					message_received = NULL;
char* 					message_to_send = NULL;
char* 					total_buffer = NULL;

void clear_clients_list() {
	t_client* tmp = clients_list;

	while (tmp) {
		clients_list = tmp->next;
		close(tmp->socket);
		free(tmp);
		tmp = clients_list;
	}
}

void error_message(char* message) {
	if (server_socket)
		close(server_socket);
	if (clients_list)
		clear_clients_list();
	write(2, message, strlen(message));
	write(2, "\n", 1);
	exit (1);
}

void server_initializer(int port) {
	struct sockaddr_in 		server_address;
	bzero(&server_address, sizeof(server_address));
	server_address.sin_family = AF_INET;
	server_address.sin_addr.s_addr = htonl(2130706433);
	server_address.sin_port = htons(port);

	if ((server_socket = socket(AF_INET, SOCK_STREAM, 0)) < 0)
		error_message("Fatal error");
	if (bind(server_socket, (struct sockaddr*)& server_address, sizeof(server_address)) < 0)
		error_message("Fatal error");
	if (listen(server_socket, 0) < 0)
		error_message("Fatal error");
	
	FD_ZERO(&fds_current);
	FD_SET(server_socket, &fds_current);
	bzero(&buffer_receptor, sizeof(buffer_receptor));
}

int get_max_socket() {
	t_client* tmp = clients_list;
	int max = server_socket;

	while (tmp) {
		(tmp->socket > max) ? max = tmp->socket : 0;
		tmp = tmp->next;
	}
	return (max);
}

int add_client_to_list(int client_socket) {
	t_client* tmp = clients_list;
	t_client* new;

	if (!(new = calloc(1, sizeof(t_client))))
		error_message("Fatal error");
	
	new->id = nb_connection++;
	new->socket = client_socket;
	new->next = NULL;

	if (!clients_list)
		clients_list = new;
	else {
		while (tmp->next)
			tmp = tmp->next;
		tmp->next = new;
	}
	return (new->id);
}

void send_notification_to_all_clients(char* notification, int socket_sender) {
	t_client* tmp = clients_list;

	while (tmp) {
		if (tmp->socket != socket_sender && FD_ISSET(tmp->socket, &fds_write)) {
			if (send(tmp->socket, notification, strlen(notification), 0) < 0)
				error_message("Fatal error");
		}
		tmp = tmp->next;
	}
}

void client_login_handler() {
	struct sockaddr_in 		client_address;
	bzero(&client_address, sizeof(client_address));
	int len = sizeof(client_address);
	int client_socket;

	if ((client_socket = accept(server_socket, (struct sockaddr*)& client_address, (socklen_t*)& len)) < 0)
		error_message("Fatal error");
	
	fcntl(client_socket, F_SETFL, O_NONBLOCK);		// a supprimer avant de push

	bzero(&notification, sizeof(notification));
	sprintf(notification, "server: client %d just arrived\n", add_client_to_list(client_socket));
	send_notification_to_all_clients(notification, client_socket);
	FD_SET(client_socket, &fds_current);
}

int get_client_id(client_socket) {
	t_client* tmp = clients_list;

	while (tmp && tmp->socket != client_socket)
		tmp = tmp->next;
	return (tmp->id);
}

int remove_client_from_list(int client_socket) {
	t_client* tmp = clients_list;
	t_client* delete;

	int id = get_client_id(client_socket);

	if (tmp && tmp->socket == client_socket) {
		clients_list = tmp->next;
		close(tmp->socket);
		free(tmp);
	}
	else {
		while (tmp && tmp->next && tmp->next->socket != client_socket)
			tmp = tmp->next;
		
		delete = tmp->next;
		tmp->next = tmp->next->next;
		close(delete->socket);
		free(delete);
	}
	return (id);
}

void client_logout_handler(int socket_to_disconnect) {
	bzero(&notification, sizeof(notification));
	sprintf(notification, "server: client %d just left\n", remove_client_from_list(socket_to_disconnect));
	send_notification_to_all_clients(notification, socket_to_disconnect);
	FD_CLR(socket_to_disconnect, &fds_current);
}

char* str_join() {
	char* new;
	int len = strlen(buffer_receptor) + 1;

	len += (message_received) ? strlen(message_received) : 0;

	if (!(new = calloc(1, sizeof(char) * len)))
		error_message("Fatal error");
	
	if (message_received) {
		strcat(new, message_received);
		free(message_received);
	}
	strcat(new, buffer_receptor);
	return (new);
}

int message_extractor() {
	char* new;
	int i = 0;

	while (message_received[i]) {
		if (message_received[i] == '\n' || message_received[i + 1] == '\0') {
			if (!(new = calloc(1, sizeof(char) * strlen(message_received + i + 1) + 1)))
				error_message("Fatal error");

			strcpy(new, message_received + i + 1);
			message_to_send = message_received;
			message_to_send[i + 1] = 0;
			message_received = new;
			return (1);
		}
		i++;
	}
	return (0);
}

void send_message_to_all_clients(int socket_sender) {
	if (!(total_buffer = calloc(1, sizeof(char) * (strlen(message_to_send) + strlen(notification)))))
		error_message("Fatal error");
	
	sprintf(total_buffer, "client %d: %s", get_client_id(socket_sender), message_to_send);
	send_notification_to_all_clients(total_buffer, socket_sender);

	free(total_buffer);
	total_buffer = NULL;
	free(message_to_send);
	message_to_send = NULL;
}

void routine() {
	for (;;) {
		fds_read = fds_write = fds_current;
		if (select(get_max_socket() + 1, &fds_read, &fds_write, NULL, NULL) < 0)
			continue ;
		for (int connection = 0 ; connection <= get_max_socket() ; connection++) {
			if (FD_ISSET(connection, &fds_read)) {
				if (connection == server_socket)
					client_login_handler();
				else {
					status = 0;
					while ((ret = recv(connection, buffer_receptor, 4095, 0)) > 0) {
						buffer_receptor[ret] = '\0';
						status += ret;
						message_received = str_join();
					}
					if (status <= 0)
						client_logout_handler(connection);
					else {
						message_to_send = NULL;
						while (message_extractor())
							send_message_to_all_clients(connection);
					}
					free(message_received);
					message_received = NULL;
				}
			}
		}
	}
}

int main(int ac, char** av) {
	if (ac != 2)
		error_message("Wrong number of arguments");
	server_initializer(atoi(av[1]));
	routine();
	return (0);
}
