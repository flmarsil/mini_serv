# mini_serv

Ecrire un programme qui écoutera les clients se connecter sur un certain port sur 127.0.0.1 et qui permettra aux clients de parler entre eux.

Compiler le programme et excecuter le binaire avec en argument un numéro de port
```
gcc -Wall -Wextra -Werror mini_serv.c && ./a.out 8080
```

Connecter différents clients et envoyer des messages via netcat
```
nc 127.0.0.1 8080
```
